export const state = () => ({
  widgets: [],
  unpinnedWidgets: [],
  loaded: false
})
export const mutations = {
  SET_WidgetState (state, data) {
    state.widgets = JSON.parse(JSON.stringify(data))
    state.loaded = true
  },
  SET_WidgetStateItem (state, { index, name, value }) {
    state.widgets[index][name] = value
  },
  FROM_PinToUnpin (state, id) {
    const index = state.widgets.findIndex(f => f.id === id)
    state.unpinnedWidgets.push(state.widgets.splice(index, 1)[0])
  },
  FROM_UnpinToPin (state, { id, top, left, width, height }) {
    const index = state.unpinnedWidgets.findIndex(f => f.id === id)
    const tmpData = state.unpinnedWidgets.splice(index, 1)[0]
    tmpData.width = width
    tmpData.height = height
    tmpData.left = left
    tmpData.top = top
    state.widgets.push(tmpData)
  }
}
export const actions = () => ({
  /* async nuxtServerInit({ commit }, { req }) {
    console.log(commit, req)
  } */

})
